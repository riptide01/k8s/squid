FROM oraclelinux:8.5

RUN yum install -y squid; mkdir /etc/squid; yum clean all
ENV TZ=Europe/Moscow

EXPOSE 3128

CMD ["/bin/sh", "-c", "squid -z -N; squid -N"]