# INSTALLATION
```bash
helm upgrade --install squid ./helm/ -f ./helm/values.yaml --namespace squid --create-namespace --atomic --timeout 600

```


# Конфигурация
`config file:` /etc/squid/squid.conf # configMap/squid-config

`proxy port:` 3128

`configure allowed to connect to proxy LAN IPs:` configMap/squid-acl
```bash
# /etc/squid/acl/localnet.acl
192.168.0.0/16
10.0.0.0/8
```


## ACL
Один ACL можно объявить несколько раз, будут учтены оба выражения

`Конфигурация access-list`:
```bash
# acl %название% [src, dst, srcdomain, dstdomain, port, method] %значение/путьКФайлу%

# Список хостов ИЗ КОТОРЫХ идет трафик
acl AdminHost src 10.0.0.1/32
# Список хостов (из файла) ИЗ КОТОРЫХ идет трафик
acl blacklist src "/etc/squid/acl/blacklist.acl"
# Список доменов В КОТОРЫЕ идет трафик
acl whitedomain dstdomain .google.com
acl whitedomain dstdomain .youtube.com
# Список для портов
acl allowed_ports port 80
acl allowed_ports port 443
# Список для методов
acl POST method POST
```


## Правила
Правила обрабатываются сверху-вниз, при первом сопадении прерывая дальнейший поиск правил, но цикл redirect идет параллельно.

`Конфигурация правил:`
```bash
# http_access [allow, deny] %кому ACL% %куда IP/ACL/domain%
# Разрешить ACL AdminHost ходить везде
http_access allow AdminHost all 
# Запретить ACL blacklist ходить везде
http_access deny blacklist
# Запретить все порты кроме перечисленных в ACL allowed_ports
http_access deny !allowed_ports 
# Запретить хостам из ACL blacklist метод из ACL POST
http_access deny POST blacklist


### Редирект на другой Прокси-сервер
# never_direct [allow, deny] %куда%
# Разрешить ACL redirect всегда перенаправлять трафик в cache_peer
never_direct allow redirect
# Запретить всем остальным переходить в другое прокси squid
never_direct deny all
# Редирект %кому% задается в cache_peer_access
```


P.S. На самом деле правила можно задавать намного шире, ознакомиться с полной версией можно по ссылке:

http://www.squid-cache.org/Doc/config/http_access/

Также может быть полезно

http://www.squid-cache.org/Doc/config/logformat/