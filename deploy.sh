kubectl apply -f ./manifest
kubectl delete po --selector app.kubernetes.io/name=squid-vpn --grace-period 0
kubectl get po
sleep 5
kubectl get po
kubectl logs --selector app.kubernetes.io/name=squid-vpn
sleep 1
kubectl exec -it daemonsets/squid-vpn -- watch "cat /var/log/squid/access.log|tail -n 50"
